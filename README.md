# NodeJS template project with Gitlab CI

Rules:
- [MUST] Check coding style before each commit
- [MUST] Setup CI to run test and create artifact

## Coding stype

Stype [https://github.com/felixge/node-style-guide](https://github.com/felixge/node-style-guide).

In order to following this styling, make sure the eslint check is run before each commit (`pre-commit` section in package.json).

## Gitlab CI

Alway setup CI with levels of tests to make your project less buggy.

When this project is a backend service, you should consider to create docker image as artifact. The CI should go in pipeline: create, check, and public.

Folling `.gitlab-ci.yml` template create an image, connect to postgres db to run testing.

```
image: docker:1.12

stages:
  - build
  - publish

variables:
  CONTAINER_TEST_IMAGE: registry.gitlab.com/nqd/coolProj:$CI_BUILD_REF_NAME
  CONTAINER_RELEASE_IMAGE: registry.gitlab.com/nqd/coolProj:latest
  NODE_ENV: ci
  POSTGRES_DB: customDB
  POSTGRES_USER: customUser
  POSTGRES_PASSWORD: customPass
  POSTGRES_URL: postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@postgres:5432/$POSTGRES_DB

before_script:
  - docker login -e "nqdinhddt@gmail.com" -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com

build_job:
  stage: build
  services:
    - docker:dind
    - postgres:9.5-alpine
  script:
    # run the postgres service
    - docker run
      --name service-postgres
      -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD
      -e POSTGRES_USER=$POSTGRES_USER
      -d postgres:9.5
    # build the image
    - docker build -t $CONTAINER_TEST_IMAGE .
    - docker run
      --link service-postgres:postgres
      -e NODE_ENV=$NODE_ENV
      -e POSTGRES_URL=$POSTGRES_URL
      -e SENDGRID_API_KEY=$SENDGRID_API_KEY
      $CONTAINER_TEST_IMAGE npm run migrate
    # run the image with postgres db attached
    - docker run
      --link service-postgres:postgres
      -e NODE_ENV=$NODE_ENV
      -e POSTGRES_URL=$POSTGRES_URL
      -e SENDGRID_API_KEY=$SENDGRID_API_KEY
      $CONTAINER_TEST_IMAGE npm run test
    # save image (branched tag) to gitlab registry
    - docker push $CONTAINER_TEST_IMAGE

publish_job:
  stage: publish
  services:
    - docker:dind
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker push $CONTAINER_RELEASE_IMAGE
  only:
    - tags
    - triggers
```

When the project is not creating an backend service, but a zip file (like for aws lambda), use the artifact creating of gitlab ci to store inside the gitlab ci pipelines.

Folling `.gitlab-ci.yml` template run a test, then create a zip artifact.
This setting does not use docker in docker, then will run faster.

```
cache:
  untracked: true
  paths:
    - node_modules/

stages:
  - unit_test
  - release

before_script:
  # zip to create artifacts
  - apt-get update && apt-get install -y zip
  # release NEED node_modules also
  - npm install

test_job:
  stage: unit_test
  script:
    - npm run syntax_test
    - npm run nsp_test
    - AWS_ACCESS_KEY_ID=bla AWS_SECRET_ACCESS_KEY=bla npm run test

release_job:
  stage: release
  script:
    - ./release.artifacts.sh
  artifacts:
    paths:
    - release/
  only:
    - tags
```

## Use nvm to control version in local development host

AWS Lambda require nodejs version 4.3.2, you will find `nvm` deadly useful.
Aways use `nvm` to set version for each project.

Example
```
$cat .nvmrc
4.3.2
```
